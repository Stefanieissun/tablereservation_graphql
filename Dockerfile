FROM node:16.13.0-alpine
WORKDIR /home/app
COPY package.json .
COPY . .
RUN npm i pnpm -g && pnpm install && pnpm run build && rm -rf src
# RUN npm i pnpm ts-node -g && pnpm install
# 设置时区 上海时区
ENV TZ Asia/Shanghai
ENV NODE_ENV docker
EXPOSE 3000
# CMD ["npm", "run", "server"]
CMD ["node", "dist/src/main.js"]
# CMD ["ts-node", "src/main.ts"]