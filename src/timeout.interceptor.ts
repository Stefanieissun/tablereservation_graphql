import {
  CallHandler,
  ExecutionContext,
  Injectable,
  // Logger,
  NestInterceptor,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { timeout } from 'rxjs/operators';
@Injectable()
export class TimeoutInterceptor implements NestInterceptor {
  // constructor() { /* TODO document why this constructor is empty */ }
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next
      .handle()
      .pipe(timeout(Number(process.env.APP_RESPONSE_TIMEOUT || 8000)));
  }
}
