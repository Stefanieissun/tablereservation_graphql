import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import * as dayjs from 'dayjs';
import { map } from 'rxjs';
import { hostname } from 'os';

export interface Feedback {
  success: boolean;
  timestamp?: string;
  result?: any;
  msg?: string;
  instance?: string;
}

/** 统一返回处理 */
@Injectable()
export class ResponseInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): any {
    return next.handle().pipe(
      map((result): Feedback => {
        // return {
        //   success: true,
        //   timestamp: dayjs().format('YYYY-MM-DD HH:mm:ss'),
        //   result,
        //   msg: context.getHandler().name,
        //   instance: hostname(),
        // };
        return result;
        //TODO:暂时没想好怎么针对graphql定义可变的schema
      }),
    );
  }
}
