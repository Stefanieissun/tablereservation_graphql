import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';

@Injectable()
export class NotificationService {
  // @Interval(1000 * 100)
  // async handleCron() {
  //   console.log('每10秒执行一次', new Date().toLocaleString());
  // }

  @Cron('0 0 1 * * *', {
    name: 'handleMeetNeverEndReboot',
    timeZone: 'Asia/Shanghai',
  })
  async handleMeetNeverEndReboot() {
    // 统计每天guest, table数据
    console.log('每天凌晨1点执行一次');
    // 把数据库中isDelete的数据超过多少天的清除掉(看产品怎么定义保留多久的删除数据)
  }
}
