/**
 * @description 客户性别
 */
export enum ESex {
  F = 'F',
  M = 'M',
}

/**
 * @description table预定状态
 */
export enum ETableReservationStatus {
  /** 预定状态 */
  confirmed = 'confirmed',
  /** 取消 */
  canceled = 'canceled',
  /** 多了预定时间并超过了一段时间.(过期状态,table被释放) */
  expired = 'expired',
  /** 结束状态(正常的预定，并且客户完成了) */
  finished = 'finished',
}

/**
 * @description table状态
 */
export enum ETableStatus {
  /** 可以对外预定 */
  free = 'free',
  /** 不可对外预定(superAdmin可以更改table状态) */
  locked = 'locked',
}

/**
 * @description 账户类型
 */
export enum EUserType {
  guest = 'guest',
  admin = 'admin',
  superAdmin = 'superAdmin',
}

/**
 * @description 账户状态
 */
export enum EUserStatus {
  locked = 'locked',
  active = 'active',
}

export enum ERole {
  admin = 'admin',
  guest = 'guest',
}
