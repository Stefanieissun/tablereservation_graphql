import {
  Field,
  ID,
  InputType,
  Int,
  ObjectType,
  registerEnumType,
} from '@nestjs/graphql';
import { IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { ERole } from '../constants/enum';
import { UserModel } from '../model';

@InputType()
export class UserLoginDto {
  @Field(() => String)
  @IsString()
  @IsNotEmpty()
  username: string;

  @Field(() => String)
  @IsString()
  @IsNotEmpty()
  password: string;
}

registerEnumType(ERole, {
  name: 'ERole',
  description: '可以授予的角色',
});
@InputType()
export class UserCreateInfoDto extends UserLoginDto {
  @Field(() => ERole)
  @IsString()
  @IsNotEmpty()
  @IsEnum(ERole)
  role: ERole;

  @Field(() => String, { nullable: true })
  @IsString()
  @IsOptional()
  guestId?: string;
}

@ObjectType()
export class UserResInfo extends UserModel {
  @Field(() => ID, { description: '预定id' })
  _id: string;

  @Field(() => Date, { nullable: true, description: '创建时间' })
  createdAt!: Date;

  @Field(() => Date, { nullable: true, description: '更新时间' })
  updatedAt!: Date;
}

@ObjectType()
export class PaginationUser {
  @Field(() => Int)
  totalCount!: number;

  @Field(() => [UserResInfo])
  result!: UserResInfo[];

  @Field(() => Int)
  currentPage!: number;

  @Field(() => Int)
  pageSize!: number;
}
