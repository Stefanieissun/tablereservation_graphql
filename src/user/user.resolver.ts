// import { Resolver, Mutation, Arg } from 'type-graphql';
import { Resolver, Mutation, Args as Arg, Query } from '@nestjs/graphql';
import { UserService } from './user.service';
import { PaginationUser, UserCreateInfoDto, UserLoginDto } from './user.entity';
import {
  ForbiddenException,
  Injectable,
  UseGuards,
  BadRequestException,
} from '@nestjs/common';
import { CurrentUser, Public, Roles } from './../decorator/meta';
import { NestJwtGuard, RolesGuard } from '../guards';
import { User } from '../types';
import { ifCanCreateUser } from './../util/util';
import { Pagination } from '../guest/guest.entity';

@Injectable()
@UseGuards(NestJwtGuard, RolesGuard)
@Roles('admin', 'superAdmin')
@Resolver()
export class UserResolver {
  constructor(private readonly userService: UserService) {}

  @Public()
  @Mutation(() => String, { description: '用户登录' })
  getToken(@Arg('login') login: UserLoginDto) {
    // TODO:可以和前端设定一个加密解密的方式，暂时不加密
    const { username, password } = login;
    return this.userService.getToken({ username, password });
  }

  // @Public()

  @Mutation(() => Boolean, { description: '创建用户登录账号和密码' })
  createUser(
    @Arg('userInfo') userInfo: UserCreateInfoDto,
    @CurrentUser() user: User,
  ) {
    const { role } = user;
    const { username, password, guestId, role: givenRole } = userInfo;
    const isRight = ifCanCreateUser(role, givenRole);
    if (!isRight) {
      throw new ForbiddenException('access deny');
    }
    if (givenRole === 'guest' && !guestId) {
      throw new BadRequestException('请输入guestId');
    }
    return this.userService.createUser({
      username,
      password,
      givenRole,
      guestId,
    });
  }

  @Mutation(() => Boolean, { description: '锁定某个账户' })
  @Roles('superAdmin')
  lockUser(@Arg('id', { description: 'mongo中的_id' }) id: string) {
    return this.userService.lockUser(id);
  }

  @Query(() => PaginationUser, { description: '获取所有登录用户的信息' })
  @Roles('admin', 'superAdmin')
  getAllUsers(
    @Arg('pagination') pagination: Pagination,
    @CurrentUser() user: User,
  ) {
    const { pageIndex, pageSize } = pagination;
    const { role } = user;
    return this.userService.getAllUsers(
      { pageIndex, pageSize },
      role === 'superAdmin',
    );
  }
}
