import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserResolver } from './user.resolver';
import { TypegooseModule } from 'nestjs-typegoose';
import { UserModel } from '../model';
import { AuthService } from '../auth/auth.service';
import { JwtService } from '@nestjs/jwt';

@Module({
  providers: [UserResolver, UserService, AuthService, JwtService],
  imports: [TypegooseModule.forFeature([UserModel])],
})
export class UserModule {}
