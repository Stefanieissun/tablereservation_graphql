import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from 'nestjs-typegoose';
import { UserModel } from '../model/user.model';
import { ReturnModelType } from '@typegoose/typegoose';
import { CryptoService } from './../shared/services';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(UserModel)
    private readonly _userModel: ReturnModelType<typeof UserModel>,

    private readonly _cryptoService: CryptoService,

    private readonly _authService: AuthService,
  ) {}
  create(createUserInput: any) {
    return 'This action adds a new user';
  }

  findAll() {
    return `This action returns all user`;
  }

  findOne(id: number) {
    return `This action returns a #${id} user`;
  }

  update(id: number, updateUserInput: any) {
    return `This action updates a #${id} user`;
  }

  remove(id: number) {
    return `This action removes a #${id} user`;
  }

  async getToken(loginDto: { username: string; password: string }) {
    const { username, password } = loginDto;
    // const pwd1 = this._cryptoService.aesEncrypt(password);
    // const name = this._cryptoService.aesEncrypt(username);
    // await this._userModel.create({
    //   username: name,
    //   password: pwd1,
    //   type: 'guest',
    //   status: 'active',
    // });
    // return;

    const encryptUsername = this._cryptoService.aesEncrypt(username);
    const encryptPassword = this._cryptoService.aesEncrypt(password);
    const data = await this._userModel
      .findOne({
        username: encryptUsername,
        password: encryptPassword,
        // status: 'active',
      })
      .lean();
    if (!data) {
      throw new BadRequestException('check username or password');
    }
    if (data.type !== 'superAdmin' && data.status === 'locked') {
      throw new BadRequestException('your account is locked');
    }
    const { _id, type } = data;
    const id = _id.toString();
    return this._authService.signPayload({ id, type });
  }

  async createUser(opt: {
    username: string;
    password: string;
    givenRole: 'admin' | 'guest';
    guestId?: string;
  }) {
    const { username, password, givenRole, guestId } = opt;
    const pwd = this._cryptoService.aesEncrypt(password);
    const name = this._cryptoService.aesEncrypt(username);
    const insertData = {
      username: name,
      password: pwd,
      type: givenRole,
      status: 'active',
    };
    if (guestId) {
      insertData['guestRef'] = guestId;
    }
    await this._userModel.create({
      username: name,
      password: pwd,
      type: 'guest',
      status: 'active',
    });
    return true;
  }

  async lockUser(userId: string) {
    await this._userModel.findOneAndUpdate(
      { _id: userId },
      { $set: { status: 'locked' } },
    );
    return true;
  }

  async getAllUsers(
    page: { pageIndex: number; pageSize: number },
    isSuperAdmin = false,
  ) {
    const { pageIndex, pageSize } = page;
    const data = await this._userModel
      .find({ isDeleted: { $ne: true } }, { isDeleted: 0 })
      .sort({ createdAt: -1 })
      .skip((pageIndex - 1) * pageSize)
      .limit(pageSize)
      .lean();
    if (data.length < 1) {
      return { totalCount: 0, result: [], currentPage: pageIndex, pageSize };
    }
    //
    isSuperAdmin &&
      data.map((x) => {
        x.username = this._cryptoService.aesDecrypt(x.username);
        x.password = this._cryptoService.aesDecrypt(x.password);
      });
    const totalCount = await this._userModel.countDocuments({
      isDeleted: { $ne: true },
    });

    return { totalCount, result: data, currentPage: pageIndex, pageSize };
  }
}
