// 扩充process.env
declare global {
  namespace NodeJS {
    interface ProcessEnv {
      APP_RESPONSE_TIMEOUT: string;
      APP_PORT: string;
      APP_TOKEN_KEY: string;
      NODE_ENV:
        | 'development'
        | 'cty'
        | 'local'
        | 'staging'
        | 'dev'
        | 'prod'
        | 'develop';
      DB_CONN_STR: string;
      REDIS_URL: string;
      MEET_REDIS_PORT: string;
      REDIS_PASSWORD: string;
      REDIS_DB: string;
      CLOUD_RENDER_REDIS_URL: string;
      CLOUD_RENDER_REDIS_PORT: string;
      CLOUD_RENDER_REDIS_PASSWORD: string;
      CLOUD_RENDER_REDIS_DB: string;
      OAUTH_DOMAIN: string;
      OAUTH_DOMAIN_FRONT: string;
      OAUTH_CLIENT_ID: string;
      OAUTH_CLIENT_SECRET: string;
      OAUTH_URL: string;
      OAUTH_GRANT_TYPE: string;
      OAUTH_USER_TYPE: string;
      OAUTH_TOKEN_VERIFY: string;
      AXIOS_RETRY: string;
      AXIOS_TIME_OUT: string;
      AXIOS_RETRY_DELAY: string;
      SIGNALING_DOMAIN: string;
      WDP_DOMAIN: string;
      WDP_GET_ACCOUNT_INFO: string;
      WDP_GET_ACCOUNT_INFO_NEW: string;
      WDP_SECRET: string;
      SEARCH_APPLY_FORM: string;
      POST_APPLY_FORM: string;
      GET_RIGHTS: string;
      GET_RIGHTS_WITH_TIME: string;
      WDP_LOGIN: string;
      WDP_AUTH_KEY: string;
      MEET_ORDER: string;
      MEET_ORDER_DETAIL: string;
      MEET_ORDER_CANCEL: string;
      MEET_ORDER_WAIT_TIME: string;
      MEET_PRICE: string;
      MEET_CALCULATE_PRICE: string;
      MEET_PAY_WAY: string;
      MEET_CREATE_ORDER: string;

      CROPTY_SECRET: string;

      ALLOW_CREATE_MEET_BEFORE_ACTIVITY;
    }
  }
}
export {};
