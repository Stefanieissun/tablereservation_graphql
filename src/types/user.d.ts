export interface User {
  id: string;
  role: 'admin' | 'superAdmin' | 'guest';
}
