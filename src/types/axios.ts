import { AxiosRequestConfig as config } from 'axios';
declare module 'axios' {
  export interface AxiosRequestConfig {
    config?: config;
    /** 最大重试次数 */
    retry?: number;
    /**  当前重试次数*/
    __retryCount?: number;
    /** 重试时间间隔 */
    retryDelay?: number;

    // requestTime?: number;
    metadata?: {
      requestTime?: number;
      responseTime?: number;
    };
  }
}
