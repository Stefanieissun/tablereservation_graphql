import {
  Injectable,
  CanActivate,
  ExecutionContext,
  Logger,
  UnauthorizedException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { IS_PUBLIC_KEY } from '../decorator/meta';
import { GqlExecutionContext } from '@nestjs/graphql';
import * as request from 'supertest';

@Injectable()
export class RolesGuard implements CanActivate {
  private readonly logger = new Logger('RolesGuard');
  constructor(private readonly reflector: Reflector) {}
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass,
    ]);
    if (isPublic) {
      return true;
    }

    let router_roles = this.reflector.get<string[]>(
      'roles',
      context.getHandler(),
    );
    if (!router_roles) {
      router_roles = this.reflector.get<string[]>('roles', context.getClass());
    }
    if (!router_roles) {
      return true;
    }
    let request;
    if (
      (context as unknown as { contextType: string }).contextType === 'graphql'
    ) {
      const ctx = GqlExecutionContext.create(context);
      request = ctx.getContext().req;
    } else {
      request = context.switchToHttp().getRequest();
    }
    const { role } = request.user;

    if (!role) {
      throw new UnauthorizedException('请先登录');
    }
    return router_roles.includes(role);
  }
}
