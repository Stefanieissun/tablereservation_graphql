import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  Logger,
  HttpStatus,
  ExecutionContext,
} from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AxiosError } from 'axios';
import * as dayjs from 'dayjs';

interface IArgumentsHost extends ArgumentsHost {
  contextType: string;
}
@Catch()
export class HttpExceptionFilter implements ExceptionFilter<HttpException> {
  logger = new Logger('HttpExceptionFilter');
  catch(exception: HttpException, host: IArgumentsHost) {
    console.error(exception);
    let ctx;
    let request;
    let response;
    if (this.checkIfGraphql(host)) {
      ctx = GqlExecutionContext.create(host as unknown as ExecutionContext);
      request = ctx.getContext().req;
      response = ctx.getContext().res;
    } else {
      ctx = host.switchToHttp(); //获取状态信息
      request = ctx.getRequest();
      response = ctx.getResponse();
    }

    let status =
      'isAxiosError' in exception
        ? (exception as unknown as AxiosError).response?.status
        : exception instanceof HttpException
        ? exception.getStatus()
        : HttpStatus.INTERNAL_SERVER_ERROR;
    if (
      exception?.message.includes('Invalid authorization') &&
      (exception as any).errorFrom === 'third api'
    ) {
      status = 400;
    }
    if (!status) {
      this.logger.warn('code error and not get status, use 500, fix now!');
      status = 500;
    }
    if (status === 401 && request.url === '/api/user/logout') {
      return response.status(200).json({ success: true });
    }

    const exceptionRes: any =
      exception instanceof HttpException ? exception.getResponse() : exception;
    let { error } = exceptionRes;
    const { message } = exceptionRes;
    if (
      Array.isArray(message) &&
      message.some((errMsg) => errMsg === 'meet org email invalid')
    ) {
      error = '10024';
    }
    if (Number.isNaN(Number(error))) {
      error = '10019';
    }
    if (status === 404 && request.url === '/api/meet/meetId/cloud/') {
      error = '10013';
    }
    const errorResponse = {
      success: false,
      timestamp: dayjs().format('YYYY-MM-DD HH:mm:ss'),
      message: message || exceptionRes,
      path: request.url,
      status,
      errCode: error,
    };
    //TODO: 日志做区分dbError, requestError, internalException
    Logger.error(
      `[${dayjs().format('YYYY-MM-DD HH:mm:ss')}] ${request.method} ${
        request.url
      }`,
      JSON.stringify(errorResponse),
      'HttpExceptionFilter',
    );
    !this.checkIfGraphql(host) && response.status(status).json(errorResponse);
  }

  private checkIfGraphql(host: IArgumentsHost) {
    return host.contextType === 'graphql';
  }
}
