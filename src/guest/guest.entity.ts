import { Field, ID, InputType, Int, ObjectType } from '@nestjs/graphql';
import {
  IsEmail,
  IsEnum,
  IsMongoId,
  IsOptional,
  IsString,
  Max,
  Min,
  IsInt,
} from 'class-validator';
import { ESex } from '../constants/enum';
import { GuestObject } from '../model';

@InputType()
export class GuestIdsInput {
  @Field(() => [ID])
  @IsMongoId({ each: true, message: 'please check id' })
  ids: string[];
}

@InputType()
export class UpdateGuestObject {
  /**客户姓名 */
  @Field(() => String, { description: '客户姓名', nullable: true })
  @IsString()
  @IsOptional()
  name?: string;

  /** 客户电话(加密) */
  @Field(() => String, { description: '客户电话', nullable: true })
  @IsString()
  @IsOptional()
  phone?: string;

  /** 客户常用地址 */
  @Field(() => String, { description: '客户常用地址', nullable: true })
  @IsOptional()
  @IsString()
  address?: string;

  /** 客户备注 */
  @Field(() => String, { description: '客户备注', nullable: true })
  @IsOptional()
  @IsString()
  remark?: string;

  /** 客户邮箱(加密) */
  @Field(() => String, { description: '客户邮箱', nullable: true })
  @IsEmail()
  @IsOptional()
  email?: string;

  /** 客户身份证(加密) */
  @Field(() => String, { description: '客户身份证', nullable: true })
  @IsString()
  @IsOptional()
  idCard?: string;

  /** 性别(F女性，M男性) */
  @Field(() => ESex, { description: '性别(F女性，M男性)', nullable: true })
  @IsEnum(ESex, { message: '性别必须是F或M' })
  @IsOptional()
  sex?: ESex;
}

@InputType()
export class UpdateGuestInput {
  @Field(() => ID)
  @IsMongoId({ message: 'please check id' })
  id: string;

  @Field(() => UpdateGuestObject)
  guestInfo: UpdateGuestObject;
}

@InputType()
export class Pagination {
  /** 第几页 */
  @Field(() => Int, { defaultValue: 1, nullable: true })
  @IsInt()
  @IsOptional()
  @Min(1)
  pageIndex: number;

  /** 每一页的大小 */
  @Field(() => Int, { defaultValue: 10, nullable: true })
  @IsInt()
  @IsOptional()
  @Max(100, { message: '最大值不能超过100' })
  @Min(1)
  pageSize: number;
}

@ObjectType()
export class PaginationGuestData {
  @Field(() => Int, { description: '总数量' })
  totalCount!: number;

  @Field(() => [GuestObject], { description: '数据' })
  result!: GuestObject[];

  @Field(() => Int, { description: '当前是第几页' })
  currentPage!: number;

  @Field(() => Int, { description: '页面数据大小' })
  pageSize!: number;
}
