import { Module } from '@nestjs/common';
import { GuestService } from './guest.service';
import { GuestResolver } from './guest.resolver';
import { TypegooseModule } from 'nestjs-typegoose';
import { GuestModel } from '../model';

@Module({
  providers: [GuestResolver, GuestService],
  imports: [TypegooseModule.forFeature([GuestModel])],
})
export class GuestModule {}
