import { Injectable, BadRequestException } from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { CryptoService } from './../shared/services';
import { GuestModel, GuestObject, InputGuest } from '../model';
import * as R from 'ramda';
import { UpdateGuestObject } from './guest.entity';
import {
  idCardDesensitization,
  phoneDesensitization,
} from '../../src/util/util';

@Injectable()
export class GuestService {
  constructor(
    @InjectModel(GuestModel)
    private readonly _guestModel: ReturnModelType<typeof GuestModel>,

    private readonly _cryptoService: CryptoService,
  ) {}
  async create(createGuestInput: InputGuest) {
    const createGuest = this._cryptoService.aesEncryptOrDecrypt(
      createGuestInput,
      ['phone', 'address', 'email', 'idCard'],
      'aesEncrypt',
    );
    const result = (await this._guestModel.create(createGuest)).toJSON();
    return this.aesDecryptAndDesensitize([
      result,
    ] as unknown as GuestObject[])[0];
  }

  private desensitizeObj<
    T extends Record<string, number | string>,
    K extends keyof T,
    P extends number | string,
  >(obj: T, keys: K[], func: ((arg: P) => P)[]) {
    const newObj = R.clone(obj);
    keys.forEach((key, index) => {
      Object.assign(newObj, {
        [key]: func[index](obj[key] as unknown as any),
      });
    });
    return newObj;
  }

  //TODO:继续抽
  async findAll(
    page: { pageSize: number; pageNum: number },
    isSuperAdmin = false,
  ) {
    const { pageSize = 10, pageNum = 1 } = page;
    const data = await this._guestModel
      .find({ isDeleted: { $ne: true } }, { isDeleted: 0 })
      .skip((pageNum - 1) * pageSize)
      .limit(pageSize)
      .lean();
    if (data.length < 1) {
      return { totalCount: 0, result: [], currentPage: pageNum, pageSize };
    }
    const totalCount = await this._guestModel.countDocuments({
      isDeleted: { $ne: true },
    });
    if (isSuperAdmin) {
      // superAdmin不脱敏
      return { totalCount, result: data, currentPage: pageNum, pageSize };
    }
    // admin脱敏
    const result = this.aesDecryptAndDesensitize(
      data as unknown as GuestObject[],
    )[0];
    return { totalCount, result, currentPage: pageNum, pageSize };
  }

  /**
   * @description 客户信息解密加脱敏(如果是超管，后台确认可以不脱敏)
   * @param data
   * @returns
   */
  private aesDecryptAndDesensitize(data: GuestObject[], isSuperAdmin = false) {
    return data.map((x) => {
      Object.assign(x, { _id: x._id.toString() });
      const result = this._cryptoService.aesEncryptOrDecrypt(
        x,
        ['phone', 'email', 'idCard', 'address'],
        'aesDecrypt',
      );
      if (isSuperAdmin) {
        return result;
      }
      return this.desensitizeObj(
        result as unknown as Record<string, string>,
        ['phone', 'idCard'],
        [phoneDesensitization, idCardDesensitization],
      );
    });
  }

  async findByIds(ids: string[], isSuperAdmin = false) {
    const data = await this._guestModel
      .find({ _id: { $in: ids }, isDeleted: { $ne: true } }, { isDeleted: 0 })
      .lean();
    if (data.length === 0) {
      throw new BadRequestException('查不到此用户');
    }
    if (isSuperAdmin) {
      return data;
    }
    return this.aesDecryptAndDesensitize(data as unknown as GuestObject[]);
  }

  async update(id: string, updateGuestInput: UpdateGuestObject) {
    const data = await this._guestModel
      .findOneAndUpdate(
        { _id: id, isDeleted: { $ne: true } },
        { $set: updateGuestInput },
        { new: true },
      )
      .lean();
    if (!data) {
      throw new BadRequestException('用户不存在');
    }
    return this.aesDecryptAndDesensitize([data as unknown as GuestObject])[0];
  }

  async removeGuests(ids: string[]) {
    await this._guestModel.updateMany(
      { _id: { $in: ids } },
      { $set: { isDeleted: true } },
    );
    return true;
  }
}
