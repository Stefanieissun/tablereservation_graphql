// import { Resolver, Query, Mutation, Arg } from 'type-graphql';
import { Resolver, Query, Mutation, Args as Arg } from '@nestjs/graphql';
import { GuestService } from './guest.service';
import { GuestObject, InputGuest } from './../model/index';
import { Injectable, UseGuards } from '@nestjs/common';
import { NestJwtGuard } from '../guards/jwt.guard';
import { RolesGuard } from '../guards/roles.guards';
import {
  GuestIdsInput,
  Pagination,
  PaginationGuestData,
  UpdateGuestInput,
} from './guest.entity';
import { CurrentUser, Roles } from '../decorator/meta';
import { User } from 'src/types';

@Resolver()
@UseGuards(NestJwtGuard, RolesGuard)
@Roles('admin', 'superAdmin')
export class GuestResolver {
  constructor(private readonly guestService: GuestService) {}

  @Mutation(() => GuestObject, {
    name: 'createGuestData',
    description: '录入客户信息',
  })
  createGuest(
    @Arg('InputGuest')
    createGuestInput: InputGuest,
  ) {
    return this.guestService.create(createGuestInput);
  }

  @Query(() => PaginationGuestData, {
    name: 'guestFindAll',
    description: '查看客户资料，分页',
  })
  findAll(
    @Arg('pagination') pagination: Pagination,
    @CurrentUser() user: User,
  ) {
    const { role } = user;
    const { pageIndex, pageSize } = pagination;
    return this.guestService.findAll(
      {
        pageNum: pageIndex,
        pageSize: pageSize,
      },
      role === 'superAdmin',
    );
  }

  @Query(() => [GuestObject], { description: '批量获取用户信息详情' })
  getGuestsByIds(
    @Arg('GuestIds') guestIds: GuestIdsInput,
    @CurrentUser() user: User,
  ) {
    const { ids } = guestIds;
    return this.guestService.findByIds(ids, user.role === 'superAdmin');
  }

  @Mutation(() => GuestObject, { description: '更新用户信息' })
  updateGuest(@Arg('updateGuestInput') updateGuestInput: UpdateGuestInput) {
    return this.guestService.update(
      updateGuestInput.id,
      updateGuestInput.guestInfo,
    );
  }

  @Mutation(() => Boolean, { description: '删除用户信息' })
  removeGuest(@Arg('GuestIds') guestIds: GuestIdsInput) {
    const { ids } = guestIds;
    return this.guestService.removeGuests(ids);
  }

  // TODO：根据电话，姓名之类的模糊查询,做一个通用接口
}
