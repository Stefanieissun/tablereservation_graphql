import {
  Injectable,
  OnModuleInit,
  OnApplicationBootstrap,
} from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { UserModel } from '../../model/user.model';
import { CryptoService } from './crypto.service';

@Injectable()
export class InitService implements OnModuleInit, OnApplicationBootstrap {
  constructor(
    @InjectModel(UserModel)
    private readonly _userModel: ReturnModelType<typeof UserModel>,

    private _cryptoService: CryptoService,
  ) {}
  async onModuleInit() {
    // console.log('init');
    // 用docker第一次启动服务时候，去注册一个superAdmin
    const count = await this._userModel.countDocuments();
    if (count === 0) {
      await this._userModel.create({
        password: this._cryptoService.aesEncrypt('123456'),
        username: this._cryptoService.aesEncrypt('admin'),
        type: 'superAdmin',
        status: 'active',
      });
    }
  }

  onApplicationBootstrap() {
    console.log('bootstarp');
  }
}
