import { HttpService } from '@nestjs/axios';
import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';
import { AxiosError, AxiosRequestHeaders, AxiosResponse, Method } from 'axios';
import { inspect } from 'util';
import { v1 as uuid } from 'uuid';

@Injectable()
export class HttpRequestService {
  private readonly logger = new Logger('HttpConfigService');
  constructor(private readonly _http: HttpService) {
    this._http.axiosRef.defaults.timeout = Number(
      process.env.AXIOS_TIME_OUT || 10000,
    );
    this._http.axiosRef.defaults.retry = Number(process.env.AXIOS_RETRY || 0);
    this._http.axiosRef.defaults.retryDelay = Number(
      process.env.AXIOS_RETRY_DELAY || 500,
    );
    //TODO: 后期这个函数可能会改。
    this.requestInterceptor();
    this.responseInterceptor();
  }

  private requestInterceptor() {
    this._http.axiosRef.interceptors.request.use((config) => {
      if (!config.headers) {
        // config.headers = { };
      }

      if (!config.metadata) {
        config.metadata = {};
      }
      config.metadata.requestTime = Date.now();
      // such as get token
      // TODO:get token if it is necessary
      // config.headers.token = 'await getToken()';
      config.headers['x-request-id'] = config.headers['x-request-id'] || uuid();
      return config;
    }, Promise.reject);
  }

  private responseInterceptor() {
    this._http.axiosRef.interceptors.response.use(
      (resSuccess) => {
        if (
          'isAxiosError' in resSuccess &&
          (resSuccess as any).isAxiosError === true
        ) {
          return Promise.reject(resSuccess);
        }

        this.logResponseSuccessTime(resSuccess);
        //ATTENTION:每个后端返回格式不一致，不能一概而论，有几个请求后端，写几个httpService
        const { code, msg } = resSuccess.data;
        if (code && typeof code === 'number' && code !== 200) {
          throw new BadRequestException(msg, msg);
        }
        return resSuccess;
      },
      (err: AxiosError) => {
        this.logFailedRequestTime(err);
        if (this.checkIfClientError(err)) {
          // 是客户端的错误，就不要重复请求了。
          return Promise.reject(err);
        }
        const { config } = err;
        if (!config || !config.retry) {
          return Promise.reject(err);
        }
        config.__retryCount = config.__retryCount || 0;
        if (config.__retryCount >= config.retry) {
          this.logger.log(
            `retry request ${config.__retryCount}  time and return  right now`,
          );
          throw new InternalServerErrorException(err.message, err.message);
        }

        config.__retryCount += 1;
        const backOff = new Promise<void>((resolve) => {
          setTimeout(
            () => resolve(err.response?.data as any),
            config.retryDelay || 1,
          );
        });

        return backOff.then(() => {
          return this.request(config as unknown as any);
        });
      },
    );
  }
  public request<URL extends string>(config: {
    method: Method;
    // url: string;
    url: URL;
    headers?: AxiosRequestHeaders;
    data?: any;
    __retryCount?: number;
    showLog?: boolean;
  }) {
    return new Promise((resolve, reject) => {
      const {
        method,
        url,
        headers = {},
        data,
        __retryCount = 1,
        showLog = false,
      } = config;
      if (!showLog) {
        this.logger.log({ url, method, headers, data }, 'HttpConfigService');
      }
      const result = this._http.request({
        method,
        url,
        headers,
        data,
        __retryCount,
      });
      result.subscribe({
        next: (data) => {
          showLog &&
            this.logger.log(
              `request ${url} success, request param: ${inspect(
                {
                  url,
                  method,
                  requestBody: data?.config?.data,
                  data: data?.data,
                  headers: data?.config?.headers,
                  status: data?.status,
                },
                false,
                null,
              )}`,
            );

          // resolve(data?.data as API[URL]);
          resolve(data?.data);
        },
        error: (err) => {
          if (showLog) {
            this.logger.log(
              `request ${url} failed, request param: ${inspect(
                {
                  url,
                  method,
                  headers,
                  body: data,
                  error: err,
                },
                false,
                null,
              )}`,
            );
          }
          reject(err);
        },
      });
    });
  }

  /**
   * @description judge if the error is client error
   * @param err AxiosError
   * @returns Boolean
   */
  private checkIfClientError(err: AxiosError) {
    if (err.name === 'BadRequestException') {
      return true;
    }
    if (
      err &&
      err.response &&
      err.response.status > 399 &&
      err.response.status < 500
    ) {
      return true;
    }
    return false;
  }

  /**
   * @description get request spent time
   * @param resSuccess AxiosResponse
   */
  private logResponseSuccessTime(resSuccess: AxiosResponse<any, any>) {
    if (resSuccess.config.metadata && resSuccess.config.metadata.requestTime) {
      (
        resSuccess.config.metadata as unknown as { responseTime: number }
      ).responseTime = Date.now();
      const duration =
        (resSuccess.config.metadata as unknown as { responseTime: number })
          .responseTime -
        (resSuccess.config.metadata as unknown as { requestTime: number })
          .requestTime;
      this.logger.log(
        `request ${resSuccess.config.url} success, duration: ${duration}ms`,
        'HttpConfigService',
      );
    }
  }

  private logFailedRequestTime(err: AxiosError) {
    if (err.config && err.config.metadata && err.config.metadata.requestTime) {
      err.config.metadata.responseTime = Date.now();
      const duration =
        err.config.metadata.responseTime - err.config.metadata.requestTime;
      const header = err.config.headers;
      this.logger.warn(
        `request url:${err.config.url} x-request-id:${
          header ? header['x-request-id'] : null
        } failed, duration: ${duration}ms retry:${
          err.config.__retryCount
        }, ${JSON.stringify({
          method: err.config.method,
          header: err.config.headers,
          bodyData: err.config.data,
        })}`,
        'HttpConfigService',
      );
    }
  }
}
