import { Injectable, Logger } from '@nestjs/common';
import * as crypto from 'crypto';
import * as R from 'ramda';

@Injectable()
export class CryptoService {
  private readonly algorithm = 'aes-128-cbc';
  private readonly logger = new Logger(CryptoService.name);

  /**
   * aes 加密
   * @param plainText
   * @returns
   */
  public aesEncrypt(plainText: string): string {
    if (!plainText) return '';
    const { key, iv } = this.aesKeyAndIv();
    const cipher = crypto.createCipheriv(this.algorithm, key, iv);
    let cText = cipher.update(plainText, 'utf-8', 'hex');
    cText += cipher.final('hex');
    return cText;
  }

  /**
   * aes 解密
   * @param encryptedText
   * @returns
   */
  public aesDecrypt(encryptedText: string): string {
    if (!encryptedText) return '';
    try {
      const { key, iv } = this.aesKeyAndIv();
      const decipher = crypto.createDecipheriv(this.algorithm, key, iv);
      let dText = decipher.update(encryptedText, 'hex', 'utf-8');
      dText += decipher.final('utf8');
      return dText;
    } catch (error) {
      this.logger.error(error);
      return null;
    }
  }

  public aesKeyAndIv() {
    return {
      key: Buffer.from(process.env.AES_KEY, 'hex'),
      iv: Buffer.from(process.env.AES_IV, 'hex'),
    };
  }

  public aesEncryptOrDecrypt<T extends object, K extends keyof T>(
    obj: T,
    keys: K[],
    operate: 'aesEncrypt' | 'aesDecrypt',
  ) {
    if (!obj) {
      return obj;
    }
    //ATTENTION:mongo中的ObjectId不可以这样拷贝,BSON类型的数据需要在调用本函数之前转换一下
    const newObj = R.clone(obj);
    keys.forEach((key) => {
      if (typeof newObj[key] === 'string') {
        Object.assign(newObj, {
          [key]: this[operate](newObj[key] as string),
        });
      }
    });
    return newObj;
  }
}
