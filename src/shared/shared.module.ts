import { HttpModule } from '@nestjs/axios';
import { Global, Module } from '@nestjs/common';
import { CryptoService } from './services/crypto.service';
import { HttpRequestService } from './services/http.service';

@Global()
@Module({
  providers: [HttpRequestService, CryptoService],
  imports: [HttpModule],
  exports: [HttpRequestService, HttpModule, CryptoService],
})
export class SharedModule {}
