import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { InjectModel } from 'nestjs-typegoose';
import { CreateTableInput, UpdateTableObject } from './entities/table.entity';
// import { UpdateTableInput } from './dto/update-table.input';
import { TableModel } from '../model/table.model';
import { ReturnModelType } from '@typegoose/typegoose';

@Injectable()
export class TableService {
  constructor(
    @InjectModel(TableModel)
    private readonly _tableModel: ReturnModelType<typeof TableModel>,
  ) {}
  create(createTableInput: CreateTableInput) {
    return this._tableModel.create(createTableInput).catch((err) => {
      if (err.message.includes(`E11000 duplicate key`)) {
        throw new BadRequestException('id 重复，请替换');
      }
      throw new InternalServerErrorException(err.message);
    });
  }

  async findAll(page: { pageIndex: number; pageSize: number }) {
    const { pageIndex, pageSize } = page;
    const data = await this._tableModel
      .find({ isDeleted: { $ne: true } }, { isDeleted: 0 })
      .sort({ createdAt: -1 })
      .skip((pageIndex - 1) * pageSize)
      .limit(pageSize)
      .lean();
    if (data.length < 1) {
      return { totalCount: 0, result: [], currentPage: pageIndex, pageSize };
    }
    const totalCount = await this._tableModel.countDocuments({
      isDeleted: { $ne: true },
    });
    return { totalCount, result: data, currentPage: pageIndex, pageSize };
  }

  findOne(id: string) {
    return this._tableModel.findOne(
      { _id: id, isDeleted: { $ne: true } },
      { isDeleted: 0 },
    );
  }

  update(id: string, tableInfo: UpdateTableObject) {
    // return `This action updates a #${id} table`;
    return this._tableModel
      .findOneAndUpdate({ _id: id, isDeleted: { $ne: true } }, tableInfo, {
        new: true,
      })
      .lean();
  }

  async remove(id: string) {
    await this._tableModel.findOneAndUpdate({ _id: id }, { isDeleted: true });
    return true;
  }
}
