import { Module } from '@nestjs/common';
import { TableService } from './table.service';
import { TableResolver } from './table.resolver';
import { TypegooseModule } from 'nestjs-typegoose';
import { TableModel } from '../model/table.model';

@Module({
  providers: [TableResolver, TableService],
  imports: [TypegooseModule.forFeature([TableModel])],
})
export class TableModule {}
