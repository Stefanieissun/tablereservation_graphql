import { Resolver, Query, Mutation, Args as Arg } from '@nestjs/graphql';
import { TableService } from './table.service';
import {
  CreateTableInput,
  PaginationTableData,
  TableInfoData,
  UpdateTableInput,
} from './entities/table.entity';
import { UseGuards } from '@nestjs/common';
import { NestJwtGuard, RolesGuard } from './../guards';
import { Roles } from './../decorator/meta';
import { Pagination } from '../guest/guest.entity';

@Resolver()
@UseGuards(NestJwtGuard, RolesGuard)
@Roles('admin', 'superAdmin')
export class TableResolver {
  constructor(private readonly tableService: TableService) {}

  @Mutation(() => TableInfoData, {
    description: '录入table信息(admin,superAdmin)',
  })
  createTable(
    @Arg('CreateTableInput')
    createTableInput: CreateTableInput,
  ) {
    return this.tableService.create(createTableInput);
  }

  @Query(() => PaginationTableData, {
    name: 'tableFindAll',
    description: '获取table信息，分页',
  })
  findAll(@Arg('pagination') pagination: Pagination) {
    const { pageIndex, pageSize } = pagination;
    return this.tableService.findAll({ pageIndex, pageSize });
  }

  @Query(() => TableInfoData, {
    name: 'getTableDetailById',
    description: '根据id获取table详情',
  })
  getTableDetail(@Arg('id') id: string) {
    return this.tableService.findOne(id);
  }

  @Mutation(() => TableInfoData, { description: '更新table录入信息' })
  updateTable(@Arg('updateTableInput') updateTableInput: UpdateTableInput) {
    const { id, tableInfo } = updateTableInput;
    return this.tableService.update(id, tableInfo);
  }

  @Mutation(() => TableInfoData, { description: '删除table' })
  removeTable(@Arg('id') id: string) {
    return this.tableService.remove(id);
  }
}
