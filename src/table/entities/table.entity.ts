import { InputType, Field, ObjectType, ID, Int } from '@nestjs/graphql';
import {
  IsBoolean,
  IsEnum,
  IsInt,
  IsMongoId,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { prop as property } from '@typegoose/typegoose';
import { TableModel } from '../../model';
import { ETableStatus } from './../../constants/enum';

@InputType('CreateTableInput')
export class CreateTableInput extends TableModel {}

@ObjectType()
export class TableInfoData extends TableModel {
  @Field(() => ID, { description: 'table id' })
  _id!: string;

  @Field(() => Date, { nullable: true, description: '创建时间' })
  createdAt!: Date;

  @Field(() => Date, { nullable: true, description: '更新时间' })
  updatedAt!: Date;
}

@InputType()
export class UpdateTableObject {
  @Field(() => String, { description: 'Table name' })
  @property({ type: String, required: true })
  @IsString()
  @IsNotEmpty()
  @IsOptional()
  name?: string;

  @Field(() => Int, { description: 'Table number' })
  @property({ type: Number, required: true, unique: true })
  @IsNumber()
  id?: number;

  @Field(() => String, { description: 'Table description', nullable: true })
  @property({ type: String, required: false })
  @IsString()
  @IsOptional()
  description?: string;

  @Field(() => Int, { description: 'Table capacity' })
  @property({
    type: Number,
    required: true,
    validate: {
      validator: (v: number) => v > 0 && Number.isInteger(v),
    },
  })
  @IsInt()
  @IsOptional()
  capacity?: number;

  @Field(() => ETableStatus, { description: 'Table status' })
  @property({ type: String, required: true, enum: ETableStatus })
  @IsEnum(ETableStatus)
  @IsOptional()
  status?: ETableStatus;

  @Field(() => Boolean, { description: 'isVIP', nullable: true })
  @property({ type: Boolean, required: true, default: false })
  @IsBoolean()
  @IsOptional()
  isVIP?: boolean;
}
@InputType()
export class UpdateTableInput {
  @Field(() => ID)
  @IsMongoId({ message: 'please check id' })
  id: string;

  @Field(() => UpdateTableObject)
  tableInfo: UpdateTableObject;
}

@ObjectType()
export class PaginationTableData {
  @Field(() => Int, { description: '总数量' })
  totalCount!: number;

  @Field(() => [TableInfoData], { description: '数据' })
  result!: TableInfoData[];

  @Field(() => Int, { description: '当前是第几页' })
  currentPage!: number;

  @Field(() => Int, { description: '页面数据大小' })
  pageSize!: number;
}
