import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiHeader,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { AppService } from './app.service';
import { AuthService } from './auth/auth.service';
import { Public, Roles } from './decorator/meta';
import { NestJwtGuard } from './guards/jwt.guard';
import { RolesGuard } from './guards/roles.guards';
import { ApiFeedbackResponse } from './decorator/response';

@Controller()
@UseGuards(NestJwtGuard, RolesGuard)
@ApiTags('app')
@Roles('admin', 'user')
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly _authService: AuthService,
  ) {}

  @Public()
  @ApiOperation({ summary: '健康检查' })
  @ApiFeedbackResponse({ type: 'string', description: 'ok' })
  @Get('/healthy/check')
  healthyCheck() {
    return 'ok';
  }
}
