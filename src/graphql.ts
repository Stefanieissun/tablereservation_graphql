/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */

export enum ESex {
  F = 'F',
  M = 'M',
}

export interface GuestInput {
  guestId: string;
  name: string;
  phone: string;
  address?: Nullable<string>;
  remark?: Nullable<string>;
  email?: Nullable<string>;
  idCard: string;
  sex: ESex;
}

export interface GuestType {
  guestId: string;
  name: string;
  phone: string;
  address?: Nullable<string>;
  remark?: Nullable<string>;
  email?: Nullable<string>;
  idCard: string;
  sex: ESex;
}

export interface IQuery {
  guestFindAll(): GuestType[] | Promise<GuestType[]>;
  guestFindOneById(guestId: string): GuestType | Promise<GuestType>;
}

export interface IMutation {
  createGuestData(InputGuest: GuestInput): GuestType | Promise<GuestType>;
  removeGuest(id: number): GuestType | Promise<GuestType>;
}

type Nullable<T> = T | null;
