import { ExtractJwt, Strategy, VerifiedCallback } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { jwtConstants } from './constants';
import { TABLE_PASSPORT_STRATEGY_JWT } from '../decorator/meta';

@Injectable()
export class JwtStrategy extends PassportStrategy(
  Strategy,
  TABLE_PASSPORT_STRATEGY_JWT,
) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      passReqToCallback: true,
      secretOrKey: jwtConstants.secret,
    });
  }

  // JWT验证 - Step 4: 被守卫调用
  async validate(req: Request, payload: any, done: VerifiedCallback) {
    // console.log(`JWT验证 - Step 4: 被守卫调用`);
    const userData = { id: payload.id, role: payload.type };
    return done(null, userData);
  }
}
