import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { jwtConstants } from './constants';
@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService) {}

  signPayload(payload: any) {
    return this.jwtService.sign(payload, {
      secret: jwtConstants.secret,
      expiresIn: '1d',
      issuer: 'Leo',
      subject: 'Stefanie',
      algorithm: 'HS256',
    });
  }
}
