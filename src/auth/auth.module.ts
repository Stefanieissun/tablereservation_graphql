import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { TABLE_PASSPORT_STRATEGY_JWT } from '../decorator/meta';
import { AuthService } from './auth.service';
import { jwtConstants } from './constants';
import { JwtStrategy } from './jwt.strategy';

@Module({
  providers: [AuthService, JwtStrategy],
  imports: [
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: {
        expiresIn: '1d',
        issuer: 'Leo',
        subject: 'Stefanie',
        algorithm: 'HS256',
      },
    }),
    PassportModule.register({ defaultStrategy: TABLE_PASSPORT_STRATEGY_JWT }),
  ],
})
export class AuthModule {}
