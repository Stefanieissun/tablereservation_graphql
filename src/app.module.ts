import { Module, MiddlewareConsumer, NestModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { loadFilesToEnv } from './config/env';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import * as winston from 'winston';
import { LogMiddleware } from './middleware';
import { ScheduleModule } from '@nestjs/schedule';
import DailyRotateFile = require('winston-daily-rotate-file');

import {
  WinstonModule,
  utilities as nestWinstonModuleUtilities,
} from 'nest-winston';
import { TypegooseModule } from 'nestjs-typegoose';
import { RedisModule } from '@liaoliaots/nestjs-redis';
import { SharedModule } from './shared/shared.module';
import { NotificationService } from './schedule/task';
import { AuthModule } from './auth/auth.module';
import { AuthService } from './auth/auth.service';
import { JwtService } from '@nestjs/jwt';
import { MockProxyMiddleware } from './middleware/proxyMiddleware';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { GuestModule } from './guest/guest.module';
import { GuestModel, UserModel } from './model';
import { TableModule } from './table/table.module';
import { UserModule } from './user/user.module';
import { GraphQLModule } from '@nestjs/graphql';
import { GqlConfigService } from '../gql.config';
import { BookModule } from './book/book.module';
import { InitService } from './shared/services/config.service';

const envFilePath = loadFilesToEnv().get(
  (process.env.NODE_ENV = process.env.NODE_ENV || 'local'),
);

@Module({
  imports: [
    GraphQLModule.forRootAsync<ApolloDriverConfig>({
      driver: ApolloDriver,
      useClass: GqlConfigService,
    }),
    GuestModule,
    TableModule,
    UserModule,
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({
      envFilePath: envFilePath,
      isGlobal: true,
      encoding: 'utf-8',
      validationOptions: {
        allowUnknown: true,
        abortEarly: true,
      },
    }),
    WinstonModule.forRoot({
      exitOnError: false,
      format: winston.format.combine(
        // winston.format.colorize(),
        winston.format.timestamp({
          format: 'HH:mm:ss YY/MM/DD',
        }),
        winston.format.label({
          label: '51MEET',
        }),
        winston.format.splat(),
        // nestWinstonModuleUtilities.format.nestLike(),
      ),
      transports: [
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.ms(),
            nestWinstonModuleUtilities.format.nestLike(),
          ),
        }),
        new DailyRotateFile({
          filename: `logs/paas-info-%DATE%.log`,
          datePattern: 'YYYY-MM-DD-HH',
          zippedArchive: true,
          maxSize: '20m',
          maxFiles: '14d',
          level: 'info',
        }),
        new DailyRotateFile({
          filename: `logs/paas-error-%DATE%.log`,
          datePattern: 'YYYY-MM-DD-HH',
          zippedArchive: true,
          maxSize: '20m',
          maxFiles: '14d',
          level: 'error',
        }),
      ],
      defaultMeta: {},
    }),
    TypegooseModule.forRoot(process.env.DB_CONN_STR),
    TypegooseModule.forFeature([GuestModel, UserModel]),
    RedisModule.forRootAsync(
      {
        useFactory: () => ({
          config: [
            {
              url: process.env.REDIS_URL || 'localhost',
              port: Number(process.env.MEET_REDIS_PORT || 6379),
              db: Number(process.env.REDIS_DB || 0),
              username: process.env.REDIS_USER_NAME || '',
              password: process.env.REDIS_PASSWORD || '',
            },
          ],
        }),
      },
      true,
    ),
    SharedModule,
    AuthModule,
    BookModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    NotificationService,
    AuthService,
    JwtService,
    InitService,
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LogMiddleware).forRoutes('*');
    consumer.apply(MockProxyMiddleware).forRoutes('*');
  }
}
