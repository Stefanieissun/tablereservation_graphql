import { applyDecorators, Type } from '@nestjs/common';
import * as dayjs from 'dayjs';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiProperty,
  getSchemaPath,
} from '@nestjs/swagger';

export class Feedback {
  @ApiProperty({
    name: 'timestamp',
    description: '时间戳',
    type: String,
    example: dayjs().format('YYYY-MM-DD HH:mm:ss'),
  })
  public timestamp!: string;

  @ApiProperty({ name: 'success', description: '是否成功', type: Boolean })
  public success!: boolean;

  @ApiProperty({ name: 'result', description: '结果' })
  public result!: any;

  @ApiProperty({
    name: 'msg',
    description: '消息',
    type: String,
    example: '飞天御剑流',
  })
  public msg!: string;

  @ApiProperty({
    name: 'instance',
    description: '集群',
    type: String,
    example: '浪客剑心',
  })
  instance!: string;
}

export const ApiFeedbackResponse = <TModel extends Type<any>>({
  model,
  type,
  description,
}: {
  model?: TModel;
  type: 'string' | 'object' | 'array' | 'boolean' | 'number';
  description?: string;
}) => {
  let result: any;
  if (type === 'array') {
    result = {
      type,
      items: { $ref: getSchemaPath(model) },
      totalCount: { type: 'number' },
    };
  } else if (type === 'object') {
    result = {
      type,
      $ref: getSchemaPath(model),
    };
  } else {
    result = {
      type,
    };
  }
  return applyDecorators(
    ApiExtraModels(Feedback),
    ApiExtraModels(model),
    ApiOkResponse({
      description,
      schema: {
        allOf: [
          { $ref: getSchemaPath(Feedback) },
          {
            properties: {
              result,
            },
          },
        ],
      },
    }),
  );
};
