import { PipeTransform } from '@nestjs/common';

export class ParseParamEnum implements PipeTransform {
  private enums: Set<string | undefined>;

  constructor(enums: string[]) {
    this.enums = new Set(enums);
  }

  transform(value: any) {
    if (this.enums.has(value)) {
      return value;
    }
    throw new Error(`value is not a valid enum value`);
  }
}
