import {
  ArgumentMetadata,
  BadRequestException,
  PipeTransform,
} from '@nestjs/common';
import * as R from 'ramda';
export class ParsePageStr2NumPipe implements PipeTransform {
  constructor(private pageSizeLimit?: number) {
    this.pageSizeLimit = pageSizeLimit || 100;
  }
  transform(value: string, metadata: ArgumentMetadata): number | undefined {
    if (R.isNil(value)) {
      return 0;
    }
    if (!this.checkNum(value)) {
      throw new BadRequestException('value is not a valid number');
    }
    return this.str2Num(value, metadata.data === 'pageSize');
  }

  private checkNum(value?: string) {
    return !isNaN(Number(value));
  }

  private str2Num(value: string, isPageSize: boolean) {
    let num = Math.floor(Math.abs(Number(value)));
    if (isPageSize) {
      num = num > this.pageSizeLimit ? this.pageSizeLimit : num;
    }
    return num;
  }
}
