import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';
import { isMongoId } from 'class-validator';
import * as R from 'ramda';
@Injectable()
export class ParseMongoObjectIdPipe implements PipeTransform {
  private readonly isRequired: boolean = false;
  constructor(required?: boolean) {
    if (required) {
      this.isRequired = required;
    }
  }
  transform(value?: string): string | undefined {
    // 如果required为true，且value为undefined或null，则抛出异常
    // 如果required为false，且value为undefined或null，则返回undefined
    // 如果required为true, value 为string， 则检查value是否为合法的mongoId，如果不是则抛出异常
    // 如果required为false, value 为string 或者 null或者undefined， 如果value是null或者undefined，则返回undefined，如果value是string，则检查value是否为合法的mongoId，如果不是则抛出异常

    if (this.isRequired && R.isNil(value)) {
      throw new BadRequestException('value is required');
    }
    if (this.isRequired && !this.checkMongoId(value)) {
      throw new BadRequestException('value is not a valid mongoId');
    }
    if (R.isNil(value)) {
      return undefined;
    }
    if (!this.checkMongoId(value)) {
      throw new BadRequestException('value is not a valid mongoId');
    }
    return value;
  }

  private checkMongoId(value?: string) {
    return isMongoId(value);
  }
}
