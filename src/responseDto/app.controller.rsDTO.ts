import { ApiProperty } from '@nestjs/swagger';
import {
  IsEnum,
  IsInt,
  IsNumber,
  IsString,
  Min,
  MinLength,
} from 'class-validator';
import { ROLES } from '../decorator/meta';

export class GetPayloadDto {
  @IsNumber()
  @IsInt()
  @Min(1)
  @ApiProperty({
    name: 'userId',
    type: Number,
    description: '用户ID',
    required: true,
    minimum: 1,
  })
  userId: number;

  @IsString()
  @MinLength(1)
  @ApiProperty({
    name: 'username',
    type: String,
    minLength: 1,
    description: '用户名',
    required: true,
    example: 'Allen',
  })
  username: string;

  @IsString()
  @MinLength(1)
  @ApiProperty({
    name: 'realName',
    type: String,
    minLength: 1,
    description: '真实姓名',
    required: true,
    example: '张三',
  })
  realName: string;

  @IsString()
  @IsEnum(ROLES)
  @ApiProperty({
    name: 'roles',
    type: String,
    description: '角色',
    required: true,
    enum: ROLES,
    example: 'admin',
  })
  roles: string;
}

export class GetPayloadResDto {
  @IsString()
  @ApiProperty({
    name: 'token',
    type: String,
    required: true,
    description: 'token',
    example: 'fdfgh[oateg;nxvnshnrhsfjsg[dgdf[[gdfjg',
  })
  token: string;
}

export class GetPayloadResDto1 extends GetPayloadDto {
  @IsString()
  @ApiProperty({
    name: 'token',
    type: String,
    required: true,
    description: 'token',
    example: 'fdfgh[oateg;nxvnshnrhsfjsg[dgdf[[gdfjg',
  })
  token: string;
}
