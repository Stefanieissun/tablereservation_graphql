import {
  Field,
  ID,
  InputType,
  ObjectType,
  registerEnumType,
} from '@nestjs/graphql';
import { modelOptions, prop as property } from '@typegoose/typegoose';
import {
  IsBoolean,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';
import { ESex } from '../constants/enum';

registerEnumType(ESex, {
  name: 'ESex',
  description: '性别',
});

@modelOptions({
  schemaOptions: { timestamps: true, versionKey: false, collection: 'guest' },
})
@ObjectType('GuestObjectType')
@InputType('GuestObjectInput')
export class GuestModel {
  /**客户姓名 */
  @Field(() => String, { description: '客户姓名' })
  @property({ required: true, type: String, trim: true })
  @IsString()
  @IsNotEmpty()
  name!: string;

  /** 客户电话(加密) */
  @Field(() => String, { description: '客户电话' })
  @property({ required: true, type: String })
  @IsString()
  @IsNotEmpty()
  phone!: string;

  /** 客户常用地址 */
  @Field(() => String, { description: '客户常用地址', nullable: true })
  @property({ required: false, type: String })
  @IsOptional()
  @IsString()
  address?: string;

  /** 客户备注 */
  @Field(() => String, { description: '客户备注', nullable: true })
  @property({ required: false, type: String })
  @IsOptional()
  @IsString()
  remark?: string;

  /** 客户邮箱(加密) */
  @Field(() => String, { description: '客户邮箱', nullable: true })
  @property({
    required: false,
    type: String,
  })
  @IsEmail()
  @IsOptional()
  email?: string;

  /** 客户身份证(加密) */
  @Field(() => String, { description: '客户身份证' })
  @property({ required: true, type: String, unique: true })
  @IsString()
  @IsNotEmpty()
  idCard!: string;

  /** 性别(F女性，M男性) */
  @Field(() => ESex, { description: '性别(F女性，M男性)' })
  @property({ required: true, type: String })
  @IsEnum(ESex, { message: '性别必须是F或M' })
  @IsNotEmpty()
  sex!: ESex;

  @Field(() => Boolean, { description: '是否是会员', nullable: true })
  @property({ required: false, type: Boolean, default: false })
  @IsBoolean()
  @IsOptional()
  isVip?: boolean;

  /** 数据库伪删除(super admin 之下都不能看到) */
  @property({ required: false, type: Boolean })
  isDeleted?: boolean;
}

@ObjectType('GuestObject')
export class GuestObject extends GuestModel {
  @Field(() => ID, { description: '客户id' })
  _id!: string;

  @Field(() => Date, { nullable: true, description: '创建时间' })
  createdAt!: Date;

  @Field(() => Date, { nullable: true, description: '更新时间' })
  updatedAt!: Date;
}

@InputType('GuestInput')
export class InputGuest extends GuestModel {}
