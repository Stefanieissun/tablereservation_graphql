// 账号相关
// 希尔顿酒店管理员,超级管理员,普通管理员,普通用户

import { Field, ObjectType, registerEnumType } from '@nestjs/graphql';
import { modelOptions, Ref, prop as property } from '@typegoose/typegoose';
import { EUserStatus, EUserType } from '../constants/enum';
import { GuestModel } from './guest.model';

registerEnumType(EUserStatus, {
  name: 'EUserStatus',
  description: '账户状态',
});

registerEnumType(EUserType, {
  name: 'EUserType',
  description: '账户类型',
});

@modelOptions({
  schemaOptions: { timestamps: true, versionKey: false, collection: 'user' },
})
@ObjectType()
export class UserModel {
  /** 用户名(需要加密) */
  @property({ required: true, type: String, unique: true })
  @Field(() => String)
  username: string;

  /** 密码(需要加密) */
  @property({ required: true, type: String })
  @Field(() => String)
  password: string;

  /** 用户类型 */
  @property({ required: false, default: 'guest' })
  @Field(() => EUserType)
  type: 'superAdmin' | 'admin' | 'guest';

  /** 用户状态 */
  @Field(() => EUserStatus)
  @property({ required: false, default: 'active' })
  status: 'active' | 'locked';

  @property({ ref: () => GuestModel, required: false })
  @Field(() => String, { nullable: true })
  guestRef?: Ref<GuestModel>;

  /** 数据库伪删除(super admin 之下都不能看到) */
  @property({ required: false, type: Boolean })
  isDeleted?: boolean;
}
