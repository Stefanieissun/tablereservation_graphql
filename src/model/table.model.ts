import {
  Field,
  Float,
  ID,
  InputType,
  Int,
  ObjectType,
  registerEnumType,
} from '@nestjs/graphql';
import { modelOptions, prop as property } from '@typegoose/typegoose';
import {
  IsBoolean,
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { ETableStatus } from '../constants/enum';
registerEnumType(ETableStatus, {
  name: 'ETableStatus',
  description: '超级管理员,管理员可以设置是否对外可预定',
});

@modelOptions({
  schemaOptions: { collection: 'table', timestamps: true, versionKey: false },
})
@ObjectType('TableObjectType')
@InputType('TableInputType')
export class TableModel {
  /** table name */
  @Field(() => String, { description: 'Table name' })
  @property({ type: String, required: true })
  @IsString()
  @IsNotEmpty()
  name!: string;

  /** table 编号 */
  @Field(() => Int, { description: 'Table number' })
  @property({ type: Number, required: true, unique: true })
  @IsNumber()
  id!: number;

  /** 描述 */
  @Field(() => String, { description: 'Table description', nullable: true })
  @property({ type: String, required: false })
  @IsString()
  @IsOptional()
  description?: string;

  /** 可以坐多少人 */
  @Field(() => Int, { description: 'Table capacity' })
  @property({
    type: Number,
    required: true,
    validate: {
      validator: (v: number) => v > 0 && Number.isInteger(v),
    },
  })
  @IsInt()
  capacity!: number;

  /** 是否对外开放 */
  @Field(() => ETableStatus, { description: 'Table status' })
  @property({ type: String, required: true, enum: ETableStatus })
  @IsEnum(ETableStatus)
  status!: ETableStatus;

  @Field(() => Boolean, { description: 'isVIP', nullable: true })
  @property({ type: Boolean, required: true, default: false })
  @IsBoolean()
  @IsOptional()
  isVIP!: boolean;

  /** 数据库伪删除(super admin 之下都不能看到) */
  @property({ required: false, type: Boolean })
  isDeleted?: boolean;

  // @Field(() => Float, { description: '价格' })
  // @property({ type: Number, required: true })
  // price!: number;

  // 属于策略，另一张表吧
  // // 最长等待时间(过期后自动取消)
  // @Field({ description: 'Max waiting time(默认小时为单位)' })
  // @property({ type: Number, required: true })
  // maxWaitingTime!: number;

  // // table最长的预定时间
  // @Field({ description: 'Max reservation time' })
  // @property({ type: Number, required: true })
  // maxReservationTime!: number;
}
