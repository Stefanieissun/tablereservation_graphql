import { Field } from '@nestjs/graphql';
import { modelOptions, Ref, prop as property } from '@typegoose/typegoose';
import { IsEnum } from 'class-validator';
import { ETableReservationStatus } from '../constants/enum';
import { GuestModel } from './guest.model';
import { TableModel } from './table.model';

@modelOptions({
  schemaOptions: {
    timestamps: true,
    versionKey: false,
    collection: 'tableReservation',
  },
})
export class TableReservationModel {
  /** 用户 */
  @Field(() => GuestModel)
  @property({ ref: () => GuestModel })
  guestRef!: Ref<GuestModel>;

  /** table */
  @Field(() => TableModel)
  @property({ ref: () => GuestModel })
  tableRef!: Ref<TableModel>;

  // 预定开始时间
  @Field()
  @property({ required: true })
  reservationStartTime: Date;

  // 约定结束时间
  @Field()
  @property({ required: true })
  reservationEndTime: Date;

  // 预定状态(最初客户预定成功后，状态为confirmed，客户取消预定后，状态为canceled，超时后，状态为expired，客户实际离开后，状态为finished)
  @property({ required: true })
  @IsEnum(ETableReservationStatus)
  status: 'confirmed' | 'canceled' | 'expired' | 'finished';
}
