import { Injectable, Logger } from '@nestjs/common';
import { HttpRequestService } from './shared/services/http.service';
import { RedisService } from '@liaoliaots/nestjs-redis';
import { Redis } from 'ioredis';

@Injectable()
export class AppService {
  private readonly logger: Logger = new Logger('AppService');
  private readonly _redisClient: Redis;
  constructor(
    private _http: HttpRequestService,
    private readonly _redisService: RedisService,
  ) {
    this._redisClient = this._redisService.getClient();
  }

  getBaidu() {
    return this._http.request({
      // url: '/user',
      url: '/user/:id',
      method: 'get',
    });
  }
}
