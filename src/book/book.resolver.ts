import { UseGuards } from '@nestjs/common';
import { Args as Arg, Mutation, Query, Resolver } from '@nestjs/graphql';
import { NestJwtGuard, RolesGuard } from '../guards';
import { Pagination } from '../guest/guest.entity';
import { BookService } from './book.service';
import { CurrentUser, Roles } from '../decorator/meta';
import { User } from '../../src/types/index';
import {
  BookTableReserveTime,
  PaginationTableReservation,
  ReserveTableInput,
  SearchTableReservationInput,
} from './book.entity';

@Resolver()
@UseGuards(NestJwtGuard, RolesGuard)
@Roles('guest', 'admin', 'superAdmin')
export class BookResolver {
  constructor(private readonly bookService: BookService) {}

  @Query(() => [BookTableReserveTime], {
    description: '查看某个table某一天被预定的时间段',
  })
  async getTableReserveInfoByDate(
    @Arg('searchTable') searchTableInfo: SearchTableReservationInput,
  ) {
    const { tableId, startDate, endDate } = searchTableInfo;
    const data = await this.bookService.getTableReserveInfoByDate({
      startDate,
      endDate,
      tableId,
    });
    return data;
  }

  //查看我的预定
  @Query(() => PaginationTableReservation, { description: '查看我的预定' })
  @Roles('guest')
  getMyBook(
    @Arg('pagination') pagination: Pagination,
    @CurrentUser() user: User,
  ) {
    const { id } = user;
    const { pageIndex, pageSize } = pagination;
    return this.bookService.getMyBook({ pageIndex, pageSize }, id);
  }

  // 预定table
  @Mutation(() => Boolean, { description: '预定table' })
  @Roles('guest')
  reserveTable(
    @Arg('reserveTable') reserveTable: ReserveTableInput,
    @CurrentUser() user: User,
  ) {
    const { id } = user;
    return this.bookService.reserveTable(reserveTable, id);
  }

  // 取消预定
  @Mutation(() => Boolean, { description: '取消预定' })
  cancelTableReserve(@Arg('id') reserveId: string, @CurrentUser() user: User) {
    const { role, id } = user;
    if (role === 'guest') {
      //只能取消自己的订单
      return this.bookService.cancelTableReserve(reserveId, id);
    } else {
      //可以取消所有订单
      return this.bookService.cancelTableReserve(reserveId);
    }
  }
}
