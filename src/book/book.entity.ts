import { Field, ID, InputType, Int, ObjectType } from '@nestjs/graphql';
import {
  IsDateString,
  isDateString,
  IsMongoId,
  IsString,
} from 'class-validator';
import { TableReservationModel } from '../model';

@ObjectType()
export class TableReservationData extends TableReservationModel {
  @Field(() => ID, { description: '预定id' })
  _id: string;

  @Field(() => Date, { nullable: true, description: '创建时间' })
  createdAt!: Date;

  @Field(() => Date, { nullable: true, description: '更新时间' })
  updatedAt!: Date;
}

@ObjectType()
export class PaginationTableReservation {
  @Field(() => Int)
  totalCount!: number;

  @Field(() => [TableReservationData])
  result!: TableReservationData[];

  @Field(() => Int)
  currentPage!: number;

  @Field(() => Int)
  pageSize!: number;
}

@InputType()
export class SearchTableReservationInput {
  @Field(() => String)
  @IsString()
  startDate: string;

  @Field(() => String)
  @IsDateString()
  endDate: string;

  @Field(() => ID)
  @IsMongoId()
  tableId: string;
}

@InputType()
export class ReserveTableInput {
  @Field(() => ID)
  @IsMongoId()
  tableId: string;

  @Field(() => String)
  @IsDateString()
  reservationStartTime: string;

  @Field(() => String)
  @IsDateString()
  reservationEndTime: string;
}

@ObjectType()
export class BookTableReserveTime {
  @Field(() => String)
  reservationStartTime: string;

  @Field(() => String)
  reservationEndTime: string;
}
