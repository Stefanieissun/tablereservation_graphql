import { Module } from '@nestjs/common';
import { BookService } from './book.service';
import { BookResolver } from './book.resolver';
import { TypegooseModule } from 'nestjs-typegoose';
import { TableReservationModel } from '../model/table_reservation.model';

@Module({
  providers: [BookResolver, BookService],
  imports: [TypegooseModule.forFeature([TableReservationModel])],
})
export class BookModule {}
