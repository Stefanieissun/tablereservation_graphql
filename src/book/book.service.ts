import { Injectable, BadRequestException } from '@nestjs/common';
import { ReturnModelType } from '@typegoose/typegoose';
import { InjectModel } from 'nestjs-typegoose';
import { TableReservationModel } from '../model';
import { ReserveTableInput } from './book.entity';

@Injectable()
export class BookService {
  constructor(
    @InjectModel(TableReservationModel)
    private readonly _tableReservationModel: ReturnModelType<
      typeof TableReservationModel
    >,
  ) {}

  async getMyBook(
    page: { pageIndex: number; pageSize: number },
    userId: string,
  ) {
    const { pageIndex, pageSize } = page;
    const data = await this._tableReservationModel
      .find({ guestRef: userId })
      .skip((pageIndex - 1) * pageSize)
      .limit(pageSize);
    if (data.length < 1) {
      return { totalCount: 0, result: [], currentPage: pageIndex, pageSize };
    }
    const count = await this._tableReservationModel.countDocuments({
      _id: userId,
    });
    return {
      totalCount: count,
      result: data,
      currentPage: pageIndex,
      pageSize,
    };
  }

  async getTableReserveInfoByDate(arg: {
    startDate: string;
    endDate: string;
    tableId: string;
  }) {
    const { startDate, endDate, tableId } = arg;
    return this._tableReservationModel
      .find(
        {
          tableRef: tableId,
          reservationStartTime: { $gte: startDate },
          reservationEndTime: { $lte: endDate },
        },
        { reservationStartTime: 1, reservationEndTime: 1, _id: 0 },
      )
      .sort({ reservationStartTime: 1 })
      .lean();
  }

  async reserveTable(reserveTable: ReserveTableInput, userId: string) {
    const { reservationEndTime, reservationStartTime, tableId } = reserveTable;
    const ifCanReserve = await this.checkTableReserveCondition(reserveTable);
    if (!ifCanReserve) {
      throw new BadRequestException('该时间段与其他时间段被预订');
    }
    try {
      await this._tableReservationModel.create({
        tableRef: tableId,
        guestRef: userId,
        reservationStartTime,
        reservationEndTime,
        status: 'pending',
      });
    } catch (error) {
      console.error(error);
      return false;
    }
    return true;
  }

  async checkTableReserveCondition(reserveTable: ReserveTableInput) {
    const { reservationEndTime, reservationStartTime, tableId } = reserveTable;
    const startTime = new Date(reservationStartTime);
    const endTime = new Date(reservationEndTime);
    const timeDiff = endTime.getTime() - startTime.getTime();
    if (timeDiff < 0) {
      throw new BadRequestException('结束时间不能早于开始时间');
    }
    const now = Date.now();
    if (now > startTime.getTime()) {
      throw new BadRequestException('开始时间不能早于当前时间');
    }
    const startTimeIso = startTime.toISOString();
    const endTimeIso = endTime.toISOString();
    const counts = await this._tableReservationModel.countDocuments({
      $or: [
        {
          tableRef: tableId,
          reservationStartTime: { $gte: startTimeIso },
          reservationEndTime: { $lte: endTimeIso },
          status: { $ne: 'canceled' },
        },
        {
          tableRef: tableId,
          reservationStartTime: { $lte: startTimeIso },
          reservationEndTime: { $gte: endTimeIso },
          status: { $ne: 'canceled' },
        },
        {
          tableRef: tableId,
          reservationStartTime: { $gte: startTimeIso, $lte: endTimeIso },
          status: { $ne: 'canceled' },
        },
        {
          tableRef: tableId,
          reservationEndTime: { $gte: startTimeIso, $lte: endTimeIso },
          status: { $ne: 'canceled' },
        },
      ],
    });
    return counts === 0;
  }

  async cancelTableReserve(reserveId: string, userId?: string) {
    const findOpt = { _id: reserveId };
    if (userId) {
      Object.assign(findOpt, { guestRef: userId });
    }
    return this._tableReservationModel.findOneAndUpdate(findOpt, {
      $set: { status: 'canceled' },
    });
  }
}
