import { InternalServerErrorException } from '@nestjs/common';
import * as fs from 'fs-extra';
import * as path from 'path';

export function loadFilesToEnv() {
  const envMap = new Map();
  const holderPath = path.join(__dirname, './env');
  const files = fs.readdirSync(holderPath);
  if (files.length === 0) {
    throw new InternalServerErrorException('not config file');
  }
  files.map((x) => {
    const envKey = x.split('.').pop();
    const filePath = path.join(__dirname, `./env/${x}`);
    envMap.set(envKey, filePath);
  });

  return envMap;
}
