import day from 'dayjs';
import utc from 'dayjs/plugin/utc';
import timezone from 'dayjs/plugin/timezone';

day.extend(utc);
day.extend(timezone);

export const formateDate = (
  date: Date | number | string,
  format = 'YYYY-MM-DD HH:mm:ss',
) => {
  return day.tz(date, 'Asia/Shanghai').format(format);
};
