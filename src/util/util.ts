//电话号码脱敏，只留下前三位和后两位
export const phoneDesensitization = (phone: string) => {
  return desensitizeStr(phone, 3, 2);
};

// 身份证信息脱敏，只留下前三位和后四位
export const idCardDesensitization = (idCard: string) => {
  return desensitizeStr(idCard, 4, 3);
};

//  str脱敏，设置展示前几位，展示后几位
export const desensitizeStr = (str: string, front: number, back: number) => {
  const newStr = str.trim();
  const len = str.length;
  if (len <= front + back) {
    return newStr;
  }
  const frontStr = newStr.slice(0, front);
  const backStr = newStr.slice(len - back, len);
  const middleStr = '*'.repeat(len - front - back);
  return frontStr + middleStr + backStr;
};

/**
 * @description superAdmin 可以创建 admin 和 guest， admin可以创建 guest
 * @param role
 * @param runRole
 * @returns
 */
export const ifCanCreateUser = (role: string, runRole: string) => {
  // TODO: 权限这块可以抽出来座位一个类，去获取，更新，删除，创建
  const roleList = ['guest', 'admin', 'superAdmin'];
  return roleList.indexOf(role) > roleList.indexOf(runRole);
};
