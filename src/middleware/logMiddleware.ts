import { Injectable, NestMiddleware, Logger } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { v1 as uuid } from 'uuid';
import * as R from 'ramda';

interface ILogContext {
  url: string;
  method: string;
  responseStatus: string | number;
  spent: string;
  headers: Record<string, any>;
  body: Record<string, any>;
  query: string | Record<string, any>;
  // params: any,
  requestIp: string;
}

@Injectable()
export class LogMiddleware implements NestMiddleware {
  logger = new Logger('LogRequestMiddleware');
  noLogApiSet = new Set([
    '/api/healthy',
    '/api/event/createEvent',
    '/api/event/createEventUE',
  ]);
  use(req: Request, res: Response, next: NextFunction) {
    const { method, headers, body, baseUrl, query } = req;
    (Logger as unknown as any).staticInstanceRef.logger.defaultMeta.traceId =
      R.pathOr(uuid(), ['headers', 'x-request-id'], req);
    const before = Date.now();
    next();
    res.on('finish', () => {
      if (!this.judgeNoLogApi(baseUrl)) {
        const after = Date.now();
        this.logContext({
          url: baseUrl,
          method,
          responseStatus: res.statusCode,
          spent: `${after - before}ms`,
          headers,
          body,
          query,
          requestIp: req.ip,
        });
      }
    });
  }

  judgeNoLogApi(baseUrl: string) {
    return this.noLogApiSet.has(baseUrl);
  }

  private logContext(logContext: ILogContext) {
    this.logger.log(JSON.stringify(logContext));
  }
}
