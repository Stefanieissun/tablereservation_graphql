import { Injectable } from '@nestjs/common';
import { Method } from 'axios';
import { NextFunction, Request, Response } from 'express';
import { HttpRequestService } from '../shared/services';

@Injectable()
export class MockProxyMiddleware {
  constructor(private _httpService: HttpRequestService) {}
  async use(req: Request, res: Response, next: NextFunction) {
    if (req.originalUrl.startsWith('/api/mock')) {
      const url =
        `http://127.0.0.1:4010` + req.originalUrl.replace('/mock', '');
      console.log('proxy url', url);
      const { host, ...header } = req.header as any;
      console.log(host);
      const resData = await this._httpService.request({
        method: req.method as Method,
        url,
        data: req.body,
        headers: header,
        showLog: true,
        __retryCount: 0,
      });
      res.setHeader('Content-type', 'application/json');
      res.end(JSON.stringify(resData));
      return;
    }
    next();
  }
}
