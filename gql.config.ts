// import { GraphqlConfig } from './common/configs/config.interface';
import { ConfigService } from '@nestjs/config';
import { ApolloDriverConfig } from '@nestjs/apollo';
import { Injectable } from '@nestjs/common';
import { GqlOptionsFactory } from '@nestjs/graphql';
import * as path from 'path';

export interface GraphqlConfig {
  playgroundEnabled: boolean;
  debug: boolean;
  schemaDestination: string;
  sortSchema: boolean;
}

@Injectable()
export class GqlConfigService implements GqlOptionsFactory {
  constructor(private configService: ConfigService) {}
  createGqlOptions(): ApolloDriverConfig {
    const schemaPath = path.join(__dirname, './src/schema.gql');
    return {
      autoSchemaFile: schemaPath,
      buildSchemaOptions: {
        numberScalarMode: 'integer',
      },
      // subscription
      installSubscriptionHandlers: true,
      debug: ['local', 'develop', 'docker'].includes(process.env.NODE_ENV),
      playground: ['local', 'develop', 'docker'].includes(process.env.NODE_ENV),
      context: ({ req }) => ({ req }),
    };
  }
}
