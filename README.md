# Typegoose

### 技术栈

- nestjs
- mongodb
- redis(暂时没有用到)
- graphql
- jwt

### 角色

- guest
- admin
- superAdmin

guest的基本用户信息在GuestModel,电话，地址，身份证等隐秘信息在db里加密存储

### 权限

guest 可以查看自己的reservation, 预定table，取消自己table预约，查看自己的table预约

superAdmin 通过createUser接口创建admin账户,guest账户， 锁定admin或者guest账号

admin可以去录入guest信息，管理table信息

不用登录就可以执行的接口@Public。比如查看

### 启动

1直接用命令ts-node src/main.ts 的话，默认NODE_ENV=local，需要自己搭建mongo,redis,配置文件在.env 

2 docker-compose up 。数据库已经塞了一条superAdmin, 账号 admin 密码 123456，

调用getToken方法获取token

### TODO

- 接入配置中心nacos之类的，

- 数据查询一些常用的方法可以封装成一个db service

- 查询pageinat相关方法查询数据List和page data可以用promise.all并发

- graphql mock数据， 如写个全局中件件 /mock/url 返回mock数据

- 全局错误设置可以展示给前端的错误信息，过滤掉后端信息，可以区分环境，local，dev可以返回堆栈信息方便后端，prod环境不可以。

### 坑
type-graphql使用之后，会让nestjs本身的middleware失效，换成@nestjs/graphql继续完成